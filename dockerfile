
##############################################################################
# Basic Docker Image
##############################################################################
FROM centos:7.9.2009 as centos.base

LABEL exfo.name="CentOS 7.9" \
      exfo.version="2021.05.14"

    # ------------------------------
    # --- Arguments/Environments ---
ENV USER=vscode
ARG GID=1000
ARG UID=$GID
ARG GROUP=$USER
#ARG DNS_SERVER=10.35.0.254
#ARG HTTP_PROXY=http://10.50.51.31:8080

    # ------------------------------
    # --- SECURITY INSTALLATION ---
RUN echo "B64DER\
-----BEGIN CERTIFICATE-----\
MIIDrzCCApegAwIBAgIQctBLdKkxF5RJpOn2dgJ3YTANBgkqhkiG9w0BAQ0FADBL\
MRMwEQYKCZImiZPyLGQBGRYDbG9jMRgwFgYKCZImiZPyLGQBGRYIYXN0ZWxsaWEx\
GjAYBgNVBAMTEWFzdGVsbGlhLUFTVERDLUNBMB4XDTE2MDYyMzA4Mjg1OFoXDTM2\
MDYyMzA4Mzg1NlowSzETMBEGCgmSJomT8ixkARkWA2xvYzEYMBYGCgmSJomT8ixk\
ARkWCGFzdGVsbGlhMRowGAYDVQQDExFhc3RlbGxpYS1BU1REQy1DQTCCASIwDQYJ\
KoZIhvcNAQEBBQADggEPADCCAQoCggEBANsG7eaknz2W9CT4zL7ytdEXLyVPF555\
7T34G5FyChdHHhREmesy2qJ7hvFuWrZkzigWBf5rVKQrv9qOppHD5lnb3H5bGPD2\
hMutCmAWOKpFrk7LJmlJF9unwSzqnqN/UTSCSFs8U1+C2kB4aX5HuS9D1s61h+e2\
P7z+pr16XCeWhuXBMVkng3xsGojVE54gPnNHd1c0dzIwahZSXEQg24k5Wz0nkw3e\
hzb4d6JutjHhiE92TqmNOhJnTk05mAfk4z1aM5Qzkc9JZWzoVP5tr+qfL4h9mn4G\
Qdm8j5og9Ed6I0SmIf6nf1FfcQy2XMUTMGNXz6K5KQnnk/sFPXy27cECAwEAAaOB\
jjCBizATBgkrBgEEAYI3FAIEBh4EAEMAQTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/\
BAUwAwEB/zAdBgNVHQ4EFgQUXY2Sq/UMLSpwowKd7mQmLrPwCHMwEgYJKwYBBAGC\
NxUBBAUCAwIAAjAjBgkrBgEEAYI3FQIEFgQUGjOjaNWN6iGVbIYKye99MMbmsJ8w\
DQYJKoZIhvcNAQENBQADggEBAETy4f/UliraALaQylynbzAOrJIvpmrGqAuSYoFB\
XcIzAvGvOTvVsSXnqvDcA2NhHQamOJ5X2J4DF8YKKWWF0Z2oW+B3HIb+B+yr8UKU\
zrJLboprAu/pJYFEm+2XSvJIB+P0SqSPX2SEhPDP8Fx+rlZFHBsijdLCJumnhUdk\
zehD3wxhrwGUbVq3zCFYzIupPIvPNTFqeSIA5C2DdkLki+qFfzhrDdxk0lYgKlnx\
3SXnW7YeUarreCvnyPlwFCKTuGkpp9QD1cSXPjeHRvUmQBMx13fxK27fdJ2KQhw8\
9cmrzgLFeK2FJBDH5dIgPLHpzlL5NtoLThAx9OnMu1VyM74=\
-----END CERTIFICATE-----\
B64DER" >> /etc/pki/ca-trust/source/anchors/astellia.der && \
    update-ca-trust enable && \
    update-ca-trust extract && \
    #echo "nameserver $DNS_SERVER" >> /etc/resolv.conf && \
    #echo "proxy=$HTTP_PROXY" >> /etc/yum.conf && \
    # ------------------------------
    # --- PACKAGES INSTALLATION --- (all packages in once RUN if possible)
    yum install -y https://repo.ius.io/ius-release-el7.rpm && \
    yum update -y && \
    # Install personnal package
    yum install -y sudo vim-enhanced mlocate dos2unix tree nmap net-tools lsof htop ccze && \
    # Clean after all yum (must be done inside each RUN yum ...)
    yum clean all && rm -rf /var/cache/yum && \
    # ------------------------------
    # --- CREATE NEW USER ---
    set -euo pipefail && \
    groupadd -g $GID -r $GROUP && \
    useradd --no-log-init -m -s /bin/bash -u $UID -g $GID $USER && \
    usermod -aG wheel $USER && \
    echo "$USER  ALL=(ALL)  NOPASSWD: ALL" > /etc/sudoers.d/$USER && \
    echo "$USER:$USER$USER" |chpasswd && \
    # ------------------------------
    # --- Optimisation with Docker Volumes ---
    mkdir -p /home/$USER/src /home/$USER/.vscode-server/extensions /home/$USER/.vscode-server-insiders/extensions && \
    chown -R $USER /home/$USER/src /home/$USER/.vscode-server /home/$USER/.vscode-server-insiders && \
    # ------------------------------
    # --- PACKAGES CONFIGURATION ---
    # Update all index (link to yum install mlocate)
    updatedb

    # ------------------------------
    # --- COPY FILES ROOT AND USER ---
# root copy
COPY --chown=root:root docker-files/.bashrc /root/.bashrc

# Set the user and the working directory
USER $USER
WORKDIR /home/$USER

# user copy inside '/usr/sbin' and './'
COPY --chown=$USER:$USER docker-files/.bashrc docker-files/.bash_profile docker-files/.first-install.sh docker-files/.gitconfig ./

# Run command which will be launch by entrypoint
CMD ["/bin/bash"]
