# Scalable UP Solution

## Introduction

It contains environment to create docker + vscode environment to auto build it on your Windows machine.

## Prerequisites

- Docker Desktop installed.
- Docker extension installed inside VSCode.
- Edit file ".devcontainer/devcontainer.json" and change "__source__=..." value as below:
```bash
"workspaceMount": "source=D:/DEV/ScalableUP,target=/home/vscode/shared,type=bind,consistency=cached",
...
"mounts": [
		"source=C:/Users/micleg1/Documents/DEV/asttrace,target=/mnt/asttrace,type=bind,consistency=cached",
...
```
- Edit file "docker-files/.first-install.sh" and change "git config parameters" value as below:
```bash
	git config --global user.email "michel.legruley@exfo.com"
	git config --global user.name "micleg1"
```
- All source folders must be at the same level as below "docker-env-dev-centos":
```bash
D:/DEV
	|-- asttrace
	|-- ScalableUP
				|-- docker-env-dev-centos
				|-- aqp
				|-- capture_unit
				|-- config_ref
				|-- controller
				|-- exfo-scalableup-ansible
				|-- flex
				|-- kafka_consumer
				|-- lib_test_auto
				|-- processing_pipeline
				|-- puc
				|-- scalable_up
				|-- tools_from_AdvQoe
				|-- etc.
```

__NB:__ "__D:/DEV/ScalableUP__" can be change in previous prerequisites

## Usage

- Open the source folder with VSCode.
- VSCode will detect a ".devcontainer" folder and will propose to reopen project inside the container.
- Wait during creation of the container (duration approximately equal to 18 minutes)
- Ending of container compilation by this:
```bash
[1078746 ms] [14:23:34] Installation completed. ms-python.python
[1078804 ms] [14:23:34] Extensions installed successfully: ms-python.python
```

## More informations

* [Link to Docker Desktop](https://www.docker.com/products/docker-desktop)
* [Link to Docker inside VSCode](https://code.visualstudio.com/docs/remote/containers)
* [Link to SSH key shared inside Docker](https://developpaper.com/using-container-development-in-vs-code)
