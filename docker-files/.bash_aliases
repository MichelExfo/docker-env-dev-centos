#!/bin/bash

# User specific aliases and functions for root only
if [ "$UID" -eq 0 ] ; then
  # For root only:
  alias rm='rm -i'
  alias cp='cp -i'
  alias mv='mv -i'
fi

# Source global definitions
if [ -f /etc/bash.bashrc ]; then
    . /etc/bash.bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# Source git completion
if [ -f ~/.git-completion/git-master/contrib/completion/git-completion.bash ]; then
    . ~/.git-completion/git-master/contrib/completion/git-completion.bash
fi

# Light method to get Git branch + status
gitbranch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

PROMPT_COMMAND=build_prompt

build_prompt() {
  EXIT=$?               # save exit code of last command
  normal='\[\e[m\]'     # colors
  black='\[\e[30m\]'
  gray='\[\e[1;30m\]'
  red='\[\e[31m\]'
  lred='\[\e[1;31m\]'
  green='\[\e[32m\]'
  lgreen='\[\e[1;32m\]'
  yellow='\[\e[33m\]'
  lyellow='\[\e[1;33m\]'
  blue='\[\e[34m\]'
  lblue='\[\e[1;34m\]'
  purple='\[\e[35m\]'
  lpurple='\[\e[1;35m\]'
  cyan='\[\e[36m\]'
  lcyan='\[\e[1;36m\]'
  white='\[\e[37m\]'
  PS1='${debian_chroot:+($debian_chroot)}'  # begin prompt

  if [ $EXIT != 0 ]; then  # add arrow color dependent on exit code
    PS1+="$red✘-$EXIT "
  else
    PS1+="$green✔ "
  fi

  if [ ! -z "$VIRTUAL_ENV" ] ; then  # add python virtual env
     PS1+="$lpurple$(echo '(${VIRTUAL_ENV##*/})')$normal "
  fi

  if [ ! -z "$X_SCLS" ] ; then  # add scl enable
     PS1+="$purple$(echo $X_SCLS | sed 's/devtoolset-/gcc/' | sed 's/rh-git/git/')$normal "
  fi

  if [ ! -z "$KUBECONFIG" ] ; then  # add k8s context
     PS1+="$lpurple$(echo $(kubectl config current-context))$normal "
  fi

  PS1+="$yellow\t$normal "  # add time

  if [ "$UID" -eq 0 ] ; then  # add root@host
    PS1+="$lred\u$normal@$red\h$normal"
  elif [ "$UID" -ge 500 ] ; then  # add user@host
    PS1+="$lgreen\u$normal@$green\h$normal"
  fi

  if [ -f ~/.git-completion/git-master/contrib/completion/git-prompt.sh ]; then  # add git completion
    . ~/.git-completion/git-master/contrib/completion/git-prompt.sh
    # see https://delicious-insights.com/fr/articles/prompt-git-qui-dechire/ for details
    export GIT_PS1_SHOWDIRTYSTATE=1 GIT_PS1_SHOWSTASHSTATE=1 GIT_PS1_SHOWUNTRACKEDFILES=1
    export GIT_PS1_SHOWUPSTREAM=verbose GIT_PS1_DESCRIBE_STYLE=branch
    PS1+="$cyan\$(__git_ps1)$normal"
  else
    PS1+="$cyan\$(gitbranch)$normal"
  fi

  PS1+=":$blue\w$normal\\$ "  # add current path + rest of prompt
}

# Improve some basic commands
alias sudo='sudo '
alias ls='ls --color'
alias ll='ls -lah'
alias l='ll'
alias dir='dir --color'
alias vdir='vdir --color'
alias grep='grep --color'
alias egrep='egrep --color'
alias fgrep='fgrep --color'
alias dmesg='dmesg -LT' #useful options: --level=err or --level=warn
alias cd..='cd ..'
alias cd...='cd ../..'
alias cd....='cd ../../..'
alias cd.....='cd ../../../..'
alias cd......='cd ../../../../..'
alias cd.......='cd ../../../../../..'
alias md='mkdir -p'
alias rd='rmdir'
alias df='df -h'
alias du='du -c -h'
alias free='free -mth'
alias h="history | grep $1"
alias p="ps aux | grep $1"
alias vi='vim'
alias q='exit'
[ ! -f ~/.bash_aliases ] && alias .bashrc='vim ~/.bashrc'
[ -f ~/.bash_aliases ] && alias .bash_aliases='vim ~/.bash_aliases'
alias .bash_profile='vim ~/.bash_profile'
alias .passwd='vim /etc/passwd'
alias .group='vim /etc/group'
alias .gitconfig='vim ~/.gitconfig'
# scl alias
alias scl_dev='(([ -d "/opt/rh/devtoolset-10" ] && scl enable devtoolset-10 bash) || ([ -d "/opt/rh/devtoolset-9" ] && scl enable devtoolset-9 bash) || ([ -d "/opt/rh/devtoolset-8" ] && scl enable devtoolset-8 bash)) & (([ -d "/opt/rh/rh-git227" ] && scl enable rh-git227 bash) && ([ -d "/opt/rh/rh-git218" ] && scl enable rh-git218 bash))'
# Copy with a progress bar
alias cpx='rsync -poghb --backup-dir=/tmp/rsync -e /dev/null --progress --'
# Improve 'nmap' function
alias nmapx='nmap -v -Pn -A'
# Improve Kubernetes function
# --> Alias kget, kuse, kctx, kns and kshell inside /etc/profile.d/exfo-proxy-password-devcontainer.sh (at every new terminal creation)
if [ ! -z "$KUBECONFIG" ] ; then
  # --> KUBECONFIG updated inside .bash_profile (at every new terminal creation)
  source <(kubectl completion bash)
  alias k=kubectl
  complete -F __start_kubectl k
  # --> KUBECONFIG updated inside .bash_profile (at every new terminal creation)
  source /opt/kubectx/completion/kubectx.bash
  source /opt/kubectx/completion/kubens.bash
fi
# Erase my k9s banner
#alias k9s=k9s
# Improve 'log' function
alias log="sudo tailf /var/log/messages | ccze -A"
logview()
{
  ccze -A < $1 | less -R
}
logtail()
{
  tail -f $1 | ccze
}
# Improve 'du' function
function dux()
{
  if [ "$#" -ne 0 ] ; then
    du -h --max-depth=1 "$1" | sort -hr
  else
    du -h --max-depth=1 "." | sort -hr
  fi
}
# Which process is using precious memory
function sysinfo()
{
  ps aux | awk '{if ($5 != 0 ) print $2,$5/(1024*1024) "MB",$6/(1024*1024) "MB",$11}' | sort -k2n | tail -n10; echo "PID VSZ RSS COMMAND"
}
# Improve 'find' function
function fxh()
{
  find . -name "*.h" -print0 | xargs -0 egrep -sn $* --color
}
function fxc()
{
  find . -name "*.c" -print0 | xargs -0 egrep -sn $* --color
  find . -name "*.cpp" -print0 | xargs -0 egrep -sn $* --color
}
function f()
{
  find . -name "*" -print0 | xargs -0 egrep -sn $* --color
}
# Improve 'mkdir + cd' function
function mkcd ()
{
    mkdir "$1"
    cd "$1"
}
# Improve 'cd' function
function up()
{
    ups=""
    for i in $(seq 1 $1) 
    do
        ups=$ups"../"
    done
    cd $ups
}
function s()
{
    pwd > ~/.save_dir
}
function i()
{
    cd "$(cat ~/.save_dir)"
}
# Improve 'zip/tar' function
function extract()
{
  if [ -f $1 ]; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       tar jf $1    ;; # bunzip2 $1
      *.rar)       unrar x $1   ;;
      *.gz)        tar zf $1    ;; # gunzip $1
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.xz)        unxz $1      ;;
      *)           echo "'$1' can not be extract via extract()" ;;
    esac
  else
    echo "'$1' invalid file"
  fi
}
# stats function
function _dspstats()
{
  GCOLOR="\e[47;32m OK/HEALTHY \e[0m"
  WCOLOR="\e[43;31m WARNING \e[0m"
  CCOLOR="\e[41;30m CRITICAL \e[0m"

  COL3=""
  ERR=""
  jj=0
  for j in $(echo "$COL4"); do
  {
    ((jj++))
    ii=0
    for i in $(echo "$COL2"); do
    {
      ((ii++))
      if [[ $ii == $jj ]]; then
      {
        if [[ $i = *[[:digit:]]* ]]; then
        {
          if [ $i -ge 95 ]; then
            if [[ $COL3 == "" ]]; then
              COL3="$(echo -e "$COL3$i% $CCOLOR")"
            else
              COL3="$(echo -e "$COL3\n$i% $CCOLOR")"
            fi
            ERR="$(echo -e $j)"
          elif [[ $i -ge 85 && $i -lt 95 ]]; then
            if [[ $COL3 == "" ]]; then
              COL3="$(echo -e "$COL3$i% $WCOLOR")"
            else
              COL3="$(echo -e "$COL3\n$i% $WCOLOR")"
            fi
            if [[ $ERR == "" ]]; then
              ERR="$(echo -e $j)"
            fi
          else
            if [[ $COL3 == "" ]]; then
              COL3="$(echo -e "$COL3$i% $GCOLOR")"
            else
              COL3="$(echo -e "$COL3\n$i% $GCOLOR")"
            fi
          fi
        }
        else # case where we write the title (first line)
          if [[ $COL3 == "" ]]; then
            COL3="$(echo -e "$COL3$i")"
          else
            COL3="$(echo -e "$COL3\n$i")"
          fi
        fi
      }
      fi
    }
    done
  }
  done

  if [[ $COL0 != "" ]]; then
    echo "$COL0"
  fi
  COL3=$(echo "$COL3")
  paste  <(echo "$COL1") <(echo "$COL3") -d' '|column -t
  export ERR; export -n COL0; export -n COL1; export -n COL2; export -n COL4
}

function stats()
{
  # get usernames
  processes=$(ps h -Led -o user,pcpu,pmem)

  # sort and count usernames
  user_processes=$(echo "$processes" | sort | uniq -c | sort -rn)
  processes_count=$(echo "$processes" | wc -l)
  user_count=$(echo "$user_processes" | wc -l)

  # character used to print bar chart
  barchr="+"

  # current min, max values [from 'ps' output]
  vmin=1
  if [[ $USER == "root" ]]; then
    vmax=$(su -c 'ulimit -u' - $(grep -r ':/bin/bash' /etc/passwd | grep -v root | cut -d: -f1 | sed -n 1p))
  else
    vmax=$(ulimit -u)
  fi
  # if PROC CNT are > 2/3 of vmax, we display an error message
  vmaxUser=$(echo "$user_processes" | awk 'BEGIN {max=0} {if($1>max) max=$1} END {print max}')
  let "vmaxUser = vmaxUser * 3 / 2"
  if [[ $vmaxUser -ge $vmax ]]; then
    ERR=$(echo "$user_processes" | awk 'BEGIN {max=0} {if($1>max) {max=$1; ERR=$2}} END {print ERR}')
  fi

  stats=$(echo "$processes" | awk '{proc[$1]+=1;cpu[$1]+=$2;mem[$1]+=$3}
                                   END {for (key in proc) printf("%s\t%s\t%s\t%s\n", key, proc[key], cpu[key], mem[key])}'|sort -k4n)

  # range of the bar graph
  dmin=1
  dmax=50

  # color steps
  cstep1="\033[32m"
  cstep2="\033[33m"
  cstep3="\033[31m"
  cstepc="\033[0m"

  # generate output
  echo -e "\e(0lqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqk\e(B"
  echo -e "\e(0x\e(B  \e[1;32m\e(0\`\e(B Processus stats / Users \e[0m                                                                 \e(0x\e(B"
  echo -e "\e(0tqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqu\e(B"
  echo "$stats" | awk --assign dmin="$dmin" --assign dmax="$dmax" \
                      --assign vmin="$vmin" --assign vmax="$vmax" \
                      --assign stats="$stats" \
                      --assign cstep1="$cstep1" --assign cstep2="$cstep2" --assign cstep3="$cstep3" --assign cstepc="$cstepc"\
                      --assign barchr="$barchr" \
                      'BEGIN {printf("|%15s |%6s |%6s |%8s |%5s%45s|\n","USERS","%CPU","%MEM","PROC CNT","< MIN", "MAX >")}
                      {
                        x=int(dmin+($2-vmin)*(dmax-dmin)/(vmax-vmin));
                        printf("|%15s |%6s |%6s |%8s |",$1,$3,$4,$2);
                        for(i=1;i<=x;i++)
                        {
                          if (i >= 1 && i <= int(dmax/3))
                            {printf(cstep1 barchr cstepc);}
                          else if (i > int(dmax/3) && i <= int(2*dmax/3))
                            {printf(cstep2 barchr cstepc);}
                          else
                            {printf(cstep3 barchr cstepc);}
                        }
                        for(i=x;i<=49;i++)
                        {
                          printf(" ");
                        }
                        print "|";
                      }'
  echo -e "\e(0mqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqj\e(B"
  #---error management---#
  if [[ $ERR != "" ]]; then
    echo -e "\e(0x\e(B \e[41;30m\e(B Too much PROC CNT \e[0m\e(0x\e(B "
    echo -e "\e(0x\E(b \e[1;31m\e(B Error  \e[0m\e(0x\e(B Release it to not block new ssh connection! Launch command below:"
    echo -e "\e(0x\e(B \e[1;31m\e(B    -   \e[0m\e(0x\e(B sudo killall -u '$ERR'"
  fi

  GCOLOR="\e[47;32m OK/HEALTHY \e[0m"
  WCOLOR="\e[43;31m WARNING \e[0m"
  CCOLOR="\e[41;30m CRITICAL \e[0m"
  B0="\n\e(0lqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqk\e(B"
  B1="\n\e(0x\e(B  \e[1;32m\e(0\`\e(B "
  E2=" \e[0m "
  E1="     \e(0x\e(B\n"
  E0="\e(0mqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqj\e(B"

  #--------Check for Load Average (current data)---------------#
  echo -e $B0$B1"System statistics"$E2"                                                      "$E1$E0
  COL0=$(uptime)
  echo -e "\e(0x\e(B \e[1;33m\e(B uptime  \e[0m\e(0x\e(B $COL0"
  OS=$(egrep '^ID=' /etc/os-release | cut -d'=' -f2 | cut -d'"' -f2)
  if [[ $OS == "centos" ]]; then
    COL0=$(rpm -qf --queryformat 'Install date: [%{installtime:date}] | [%{name}]-[%{version}]-[%{release}]\n' /etc/os-release)" | Version: "$(lsb_release -d|cut -d ' ' -f2-)
  else
    COL0="Install date: "$(ls -l /etc/machine-id | awk '{print $6" "$7" "$8}')" | Last upgrade: "$(ls -l /var/lib/apt/periodic/update-success-stamp | awk '{print $6" "$7" "$8}')" | Version: "$(lsb_release -d|cut -d ' ' -f2-)
  fi
  echo -e "\e(0x\e(B \e[1;33m\e(B OS info \e[0m\e(0x\e(B $COL0"
  #---error management---#
  if [[ $(echo -e "$COL0"|wc -l)>1 ]]; then
    echo -e "\e(0x\e(B \e[41;30m\e(B Too much OS \e[0m\e(0x\e(B "
    echo -e "\e(0x\e(B \e[1;31m\e(B Error  \e[0m\e(0x\e(B Keep only once OS! Launch command below:"
    echo -e "\e(0x\e(B \e[1;31m\e(B    -   \e[0m\e(0x\e(B rpm -qa | grep $(rpm -qf --queryformat '[%{NAME}]-[%{VERSION}]' /etc/os-release)"
    echo -e "\e(0x\e(B \e[1;31m\e(B    -   \e[0m\e(0x\e(B yum downgrade redhat-release or yum upgrade redhat-release + yum remove OS obsolete package"
  fi

  #--------Check CPU usage-------------------------------------#
  echo -e $B0$B1"CPU Usage"$E2"           ( Display all CPU > 5% )                           "$E1$E0

  CPU=$(\mpstat -P ALL) # If not available: yum install sysstat
  COL0=$(echo "$CPU"|head -n 1|awk '{print}')
  IS_CENTOS=$(echo "$CPU"|tail -n +3|grep PM|wc -l)
  if [[ $IS_CENTOS -gt 0 ]]; then
    # ..... diplay all CPU > 5% .....
    #COL1=$(echo "$CPU"|tail -n +3|awk '{if ($3=="CPU"||$3=="all") {print "| "$3" | "$4" | "$5" | "$6" | "$7" | "$8" | "$9" | "$10" | "$11" | "$12" | "$13" | "} else {if ($4>5) {print "| "$3" | "$4" | "$5" | "$6" | "$7" | "$8" | "$9" | "$10" | "$11" | "$12" | "$13" | "};}}')
    #COL2=$(echo "$CPU"|tail -n +3|awk '{if ($3=="CPU") {print "Use"} else {if ($3=="all"||$4>5) {x=int(100-$13); print x};}}')
    #COL4=$(echo "$CPU"|tail -n +3|awk '{if ($3=="CPU") {print "Use"} else {if ($3=="all"||$4>5) {print $4};}}')
    # ..... display average of all CPU > 5% .....
    COL1=$(echo "$CPU"|tail -n +3|awk '{if ($3=="CPU"||$3=="all") {print "| "$3" | "$4" | "$5" | "$6" | "$7" | "$8" | "$9" | "$10" | "$11" | "$12" | "$13" | "};}')
    COL2=$(echo "$CPU"|tail -n +3|awk '{if ($3=="CPU") {print "Use"} else {if ($3=="all"&&$4>5) {x=int(100-$13); print x};}}')
    COL4=$(echo "$CPU"|tail -n +3|awk '{if ($3=="CPU") {print "Use"} else {if ($3=="all"&&$4>5) {print $4};}}')
  else
    # ..... diplay all CPU > 5% .....
    #COL1=$(echo "$CPU"|tail -n +3|awk '{if ($2=="CPU"||$2=="all") {print "| "$2" | "$3" | "$4" | "$5" | "$6" | "$7" | "$8" | "$9" | "$10" | "$11" | "$12" | "} else {if ($3>5) {print "| "$2" | "$3" | "$4" | "$5" | "$6" | "$7" | "$8" | "$9" | "$10" | "$11" | "$12" | "};}}')
    #COL2=$(echo "$CPU"|tail -n +3|awk '{if ($2=="CPU") {print "Use"} else {if ($2=="all"||$3>5) {x=int(100-$12); print x};}}')
    #COL4=$(echo "$CPU"|tail -n +3|awk '{if ($2=="CPU") {print "Use"} else {if ($2=="all"||$3>5) {print $3};}}')
    # ..... display average of all CPU > 5% .....
    COL1=$(echo "$CPU"|tail -n +3|awk '{if ($2=="CPU"||$2=="all") {print "| "$2" | "$3" | "$4" | "$5" | "$6" | "$7" | "$8" | "$9" | "$10" | "$11" | "$12" | "};}')
    COL2=$(echo "$CPU"|tail -n +3|awk '{if ($2=="CPU") {print "Use"} else {if ($2=="all"&&$3>5) {x=int(100-$12); print x};}}')
    COL4=$(echo "$CPU"|tail -n +3|awk '{if ($2=="CPU") {print "Use"} else {if ($2=="all"&&$3>5) {print $3};}}')
  fi
  export COL0; export COL1; export COL2; export COL4
  _dspstats
  #---error management---#
  if [[ $ERR != "" ]]; then
    echo -e "\e(0x\e(B \e[41;30m\e(B Top 5 CPU Resources \e[0m\e(0x\e(B "
    ps -eo pcpu,pid,ppid,user,stat,args --sort=-pcpu|grep -v $$|head -6
  fi
  export -n ERR

  #--------Check Memory usage----------------------------------#
  echo -e $B0$B1"Memory Usage"$E2"        ( OK/HEALTHY < 85% < WARNING < 95% < CRITICAL )    "$E1$E0

  MEMORYH=$(\free -ht|tail -n +2)
  MEMORY=$(\free -t|tail -n +2)
  COL0=""
  COL1=$(echo "$MEMORYH"|awk 'BEGIN {print "| - | Total | Free | "} {print "| "$1" | "$2" | "$4" | "}')
  COL2=$(echo "$MEMORY"|awk 'BEGIN {print "Use"} {x=int(100*$3/$2); print x}')
  COL4=$(echo "$MEMORY"|awk 'BEGIN {print "Use"} {print $2}')
  export COL0; export COL1; export COL2; export COL4
  _dspstats
  #---error management---#
  if [[ $ERR != "" ]]; then
    echo -e "\e(0x\e(B \e[41;30m\e(B Top 5 Memory Resources \e[0m\e(0x\e(B "
    ps -eo pmem,pid,ppid,user,stat,args --sort=-pmem|grep -v $$|head -6
  fi
  export -n ERR

  #--------Check Inode usage-----------------------------------#
  echo -e $B0$B1"INode Usage"$E2"         ( OK/HEALTHY < 85% < WARNING < 95% < CRITICAL )    "$E1$E0

  IUSAGE=$(df -iPThl -x tmpfs -x iso9660 -x devtmpfs -x squashfs|awk '!seen[$1]++'|tail -n +1)
  COL0=""
  COL1=$(echo "$IUSAGE"|awk '{print "| "$1" | "$7" | "$3" | "$5" | "}'|sed -e 's/Inodes/ITotal/g')
  COL2=$(echo "$IUSAGE"|awk '{if ($6=="-") printf("0\n"); else print $6}'|sed -e 's/%//g')
  COL4=$(echo "$IUSAGE"|awk '{print $7}')
  export COL0; export COL1; export COL2; export COL4
  _dspstats
  #---error management---#
  if [[ $ERR != "" ]]; then
    echo -e "\e(0x\e(B \e[41;30m\e(B Cache too much full \e[0m\e(0x\e(B "
    echo -e "\e(0x\e(B \e[1;31m\e(B Error  \e[0m\e(0x\e(B We must clean the cache! Launch command below:"
    echo -e "\e(0x\e(B \e[1;31m\e(B    -   \e[0m\e(0x\e(B sync;echo 3 > /proc/sys/vm/drop_caches"
  fi
  export -n ERR

  #--------Check disk usage on all mounted file systems--------#
  echo -e $B0$B1"Disk Usage"$E2"          ( OK/HEALTHY < 85% < WARNING < 95% < CRITICAL )    "$E1$E0

  FS_USAGE=$(df -PThl -x tmpfs -x iso9660 -x devtmpfs -x squashfs|awk '!seen[$1]++'|tail -n +1)
  COL0=""
  COL1=$(echo "$FS_USAGE"|awk '{print "| "$1" | "$7" | "$3" | "$5" | "}'|sed -e 's/Size/Total/g'|sed -e 's/Avail/Free/g')
  COL2=$(echo "$FS_USAGE"|awk '{print $6}'|sed -e 's/%//g')
  COL4=$(echo "$FS_USAGE"|awk '{print $7}')
  export COL0; export COL1; export COL2; export COL4
  _dspstats
  #---error management---#
  if [[ $ERR != "" ]]; then
    echo -e "\e(0x\e(B \e[41;30m\e(B Analyze $ERR disk \e[0m\e(0x\e(B "
    sudo du -h --exclude=/proc --max-depth=1 $ERR | sort -hr
  fi
  export -n ERR
}

