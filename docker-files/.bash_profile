# .bash_profile

# Always apply except inside a VSCODE which use TERM_PROGRAM variable
#if [[ "$(id -u -n)" == "vscode" ]] ; then
if [[ "$TERM_PROGRAM" != "vscode" ]] ; then
  # Activate devtoolset by default
  #[ -d "/opt/rh/devtoolset-10" ] && echo "devtoolset-10 (gcc10, ...) Activated (cf. line 4 in .bash_profile)" && source scl_source enable devtoolset-10
  [ ! -d "/opt/rh/devtoolset-10" ] && [ -d "/opt/rh/devtoolset-9" ] && echo "devtoolset-9 (gcc9, ...) Activated (cf. line 5 in .bash_profile)" && source scl_source enable devtoolset-9
  [ ! -d "/opt/rh/devtoolset-10" ] && [ ! -d "/opt/rh/devtoolset-9" ] && [ -d "/opt/rh/devtoolset-8" ] && echo "devtoolset-8 (gcc8, ...) Activated (cf. line 6 in .bash_profile)" && source scl_source enable devtoolset-8
  # Activate rh-git by default
  [ -d "/opt/rh/rh-git227" ] && echo "rh-git227 (git227) Activated (cf. line 8 in .bash_profile)" && source scl_source enable rh-git227
  [ ! -d "/opt/rh/rh-git227" ] && [ -d "/opt/rh/rh-git218" ] && echo "rh-git218 (git218) Activated (cf. line 9 in .bash_profile)" && source scl_source enable rh-git218
fi

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH

# Set KUBECONFIG path
# --> Case "one cluster" per file
export KUBECTX_SUBFOLDER=".kube"
# --> Case "all clusters" inside the same file
#export KUBECTX_SUBFOLDER=""

# Update KUBECONFIG (only for first terminal session of the user)
touch ~/$KUBECTX_SUBFOLDER/k8s-kubeconfig
if [[ $(users | grep -o $(whoami) | wc -w) -eq 1 ]]; then
  echo -e "\e(0lqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqk\e(B"; echo -e "\e(0x\e(B\e[1;31m AUTO UPDATE ~/$KUBECTX_SUBFOLDER/k8s-kubeconfig:\e[0m from /var dir   \e(0x\e(B"; echo -e "\e(0mqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqj\e(B"
  cp /var/k8s-kubeconfig ~/$KUBECTX_SUBFOLDER/k8s-kubeconfig
fi

# --> Case "one cluster" per file
[[ -n $KUBECTX_SUBFOLDER ]] && export KUBECONFIG="$HOME/$KUBECTX_SUBFOLDER/k8s-kubeconfig:/home/mly/.kube/up-performance-test-bench-config.yaml:/home/mly/.kube/ats-07-cluster-fe-up-config.yaml:/home/mly/.kube/ats-07-cluster-fe-cp-config.yaml:/home/mly/.kube/ats-07-cluster-bck-config.yaml:/home/mly/.kube/ats-07-cluster-adm-config.yaml:/home/mly/.kube/k8s-scalable-up-04-config.yaml:/home/mly/.kube/ats-03-cluster-adm-01-config.yaml:/home/mly/.kube/ats-03-cluster-bck-01-config.yaml:/home/mly/.kube/ats-03-cluster-fe-02-config.yaml:/home/mly/.kube/k8s-scalable-up-02-config.yaml:/home/mly/.kube/flex-05-config.yaml:/home/mly/.kube/reveal-02-backend-cluster-config.yaml:/home/mly/.kube/reveal-02-frontend-cluster-config.yaml:/home/mly/.kube/reveal-01-frontend-cluster-config.yaml:/home/mly/.kube/reveal-01-backend-cluster-config.yaml"

# --> Case "all clusters" inside the same file
[[ ! -n $KUBECTX_SUBFOLDER ]] && export KUBECONFIG=$HOME/$KUBECTX_SUBFOLDER/k8s-kubeconfig


# Update RUST env
#. "$HOME/.cargo/env"
