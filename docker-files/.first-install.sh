#!/bin/sh

INPUT_ARG=$1

if [[ -z "${FIRST_INSTALL}" ]]; then

	echo "\n=====> Install personnal package <=====\n"
	yum install -y sudo vim-enhanced mlocate dos2unix tree nmap net-tools lsof htop ccze
	
	echo "\n=====> Install python3 + pip3 <=====\n"
	sudo yum install -y python3

	echo "\n=====> Install pip <=====\n"
	sudo yum -y install python-pip
	sudo pip3 install --upgrade pip #--proxy=http://10.50.51.31:8080

	#echo "\n=====> Install robotframework packages <=====\n"
	# robotframework (more information here: http://planet.astellia.loc/jcms/p_156341/fr/environment-for-creating-automatic-tests-with-robotframework)
	#sudo pip install robotframework robotframework-csvlibrary robotframework-difflibrary robotframework-sshlibrary robotframework-requests robotframework-sudslibrary pandas #--proxy=http://10.50.51.31:8080
	sudo pip3 install psutil robotframework robotframework-sshlibrary robotframework-requests robotframework-sudslibrary3 pandas #--proxy=http://10.50.51.31:8080

	echo "\n=====> Intall Wireshark to use tshark in TA <=====\n"
	sudo yum install -y wireshark
	sudo ln -sf /usr/sbin/tshark /usr/sbin/tshark_testauto

	echo "\n=====> Autofs <=====\n"
	sudo ln -sf /mnt/asttrace /autofs/asttrace

	echo "\n=====> git tools <=====\n"
	sudo yum -y install https://packages.endpointdev.com/rhel/7/os/x86_64/endpoint-repo.x86_64.rpm
	sudo yum install -y git
	#sudo yum install -y rh-git218 #CentOS 7.6
	#sudo yum install -y rh-git227 #CentOS 7.7 and 7.9
	git config --global user.email "michel.legruley@exfo.com"
	git config --global user.name "micleg1"
	wget https://github.com/git/git/archive/refs/heads/master.zip
	unzip -d ./.git-completion master.zip
	rm -rf master.zip

	echo "\n=====> Intall gcc, gdb, gcc 10, gdb 10, ... : to launch it under virtual environment --> 'scl enable devtoolset-10 bash' <=====\n"
	sudo yum install -y centos-release-scl cmake cppcheck autoconf automake libtool patch gcc-c++ gdb lcov sloccount cmake3
	#sudo yum install -y devtoolset-8
	#sudo yum install -y devtoolset-9
	sudo yum install -y devtoolset-10
	# cmake not available inside devtoolset, so I prefer use a recent version
	sudo ln -sf /usr/bin/cmake3 /opt/rh/devtoolset-10/root/bin/cmake3
	# Line below activate by default "devtoolset-10" for all users !
	if [[ $INPUT_ARG = "forcedevlink" ]]; then
		sudo [ -e "/usr/bin/gdb" ] && mv /usr/bin/gdb /usr/bin/gdb4
		sudo [ -e "/usr/bin/gcc" ] && mv /usr/bin/gcc /usr/bin/gcc4
		sudo [ -e "/usr/bin/g++" ] && mv /usr/bin/g++ /usr/bin/g++4
		sudo ln -sf /opt/rh/devtoolset-10/root/usr/bin/gdb /usr/bin/gdb
		sudo ln -sf /opt/rh/devtoolset-10/root/usr/bin/gcc /usr/bin/gcc
		sudo ln -sf /opt/rh/devtoolset-10/root/usr/bin/g++ /usr/bin/g++
	fi

	echo "\n=====> Install packages for vscode extension <=====\n"
	sudo yum install -y rpmlint
	sudo pip3 install pylint #--proxy=http://10.50.51.31:8080 
	
	#echo "\n=====> Install Gitlab Runner <=====\n"
	# source here: https://docs.gitlab.com/runner/install/linux-repository.html
	#curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
	#sudo yum install -y gitlab-runner

	echo "\n=====> Install docker <=====\n"
	sudo yum install -y docker

	echo "\n=====> Install packages for AQP <=====\n"
	sudo yum install -y libpcap-devel doxygen protobuf-c-devel openssl-devel cyrus-sasl-devel

	echo "\n=====> Install packages for PUC <=====\n"
	#sudo yum install -y meson
	sudo yum install -y epel-release*.rpm
	sudo yum install -y ninja*.rpm
	sudo yum install -y meson*.rpm

	# Clean after all yum (must be done inside each RUN yum ...)
	sudo yum clean all && sudo rm -rf /var/cache/yum

	export FIRST_INSTALL="DONE"

	echo "'${FIRST_INSTALL}'\n"

else

	echo "First installation: '${FIRST_INSTALL}'\n"
fi
